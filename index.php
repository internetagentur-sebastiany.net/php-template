<?php
// kate: space-indent on; tab-width 4; indent-width 4; mixed-indent off; replace-tabs on; indent-mode cstyle;
error_reporting (E_ALL | E_STRICT);
ini_set('display_errors',1);

require_once('./src/class_template.php');
require_once('./src/class_section.php');

$template                   = new section();

/** if set to 'true' you will see placeholders
 * without values after display
 **/
$template->keep_unassigned  = false;

/** Read one or more template files **/
$template->read('./templates/head.html');
$template->read('./templates/main.html');


/** simply fetch the content of a section
 * for further processing
 */
$head = $template->fetch('Head');


/** or prepare to display it **/
$template->assign_section('Head');


/** assign a placeholder **/
$template->assign('LINKTEXT','sebastiany.net');
$template->assign_section('Body');