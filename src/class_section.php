<?php
// kate: space-indent on; tab-width 4; indent-width 4; mixed-indent off; replace-tabs on; indent-mode cstyle;
class section extends template{

    public function read(string $template){
        parent::__construct();
        parent::read($template);
    }

    protected function _preparse(){

        parent::_preparse();

        preg_match_all('~<!--sebastiany\.net::(.*)::Start-->(.*)<!--sebastiany\.net::(\1)::End-->~UisS',$this->_templates[$this->_current],$secs);

        if(empty($secs[1]) or empty($secs[2])){
            trigger_error('No section found',  E_USER_WARNING);
            return false;
        }

        $this->_sections = array_merge($this->_sections, array_combine(array_values($secs[1]),$secs[2]));
        return true;
    }

    public function assign_section(string $section){

        if(!isset($this->_sections[$section])){
            trigger_error ( 'No such Section : ' . $section ,  E_USER_NOTICE);
            return false;
        }
        $this->_templates = array($this->_sections[$section]);
        parent::display();
    }

    //wraper to assign_section();
    public function display_section(string $section){
        $this->assign_section($csection);
    }

    //wraper to assign_section();
    public function section_assign(string $csection){
        $this->assign_section($csection);
    }

    public function fetch(string $section){
        if(strlen($section) == 0){
            trigger_error('No section given',  E_USER_ERROR);
            return false;
        }

        if(!isset($this->_sections[$section])){
            trigger_error ( 'No such Section : ' . $section ,  E_USER_NOTICE);
            return false;
        }

        $this->_templates = array($this->_sections[$section]);
        parent::_replace();

        return $this->_output;
    }

    public function get_sections(){
        return array_keys($this->_sections);
    }

    public function section_exists(string $section){
        return isset($this->_sections[$section]);
    }

    public function get_content(){
        $tmp = ob_get_contents() ;
        ob_clean();
        return $tmp;
    }

    public function display(){
        ob_flush();
    }

    public function  __destruct(){
        if(ob_get_length() == 0){
            return;
        }else{
            ob_end_flush ();
        }
    }
}