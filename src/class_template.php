<?php
// kate: space-indent on; tab-width 4; indent-width 4; mixed-indent off; replace-tabs on; indent-mode cstyle;

class template{

    protected array     $_templates = array();
    protected string    $_current;
    protected array     $_placeholder = array();
    protected array     $_sections = array();
    protected string    $_output = '';

    public function  __construct(string $encoding='pass'){
        //set this to "1" for debugging only !
        ob_implicit_flush(0);

        //could be iso-8859-1, utf-8, etc..
        mb_http_output($encoding);

        ob_start("mb_output_handler");
    }

    public function read(string $template){

        if(isset($this->_templates[$template]))
            return true;

        $this->_current = (string) $template;
        return $this->_preparse();
    }

    protected function _preparse(){

        try {
            $this->_templates[$this->_current] = file_get_contents($this->_current);
        } catch (Exception $e) {
            trigger_error ('Exception: '. $e->getMessage() ,E_USER_ERROR );
            return false;
        }

        preg_match_all('~{[^\s].*}~UisS',$this->_templates[$this->_current],$matches);

        if(!empty($matches[0])){
            $this->_placeholder = array_merge($this->_placeholder,array_combine(($matches[0]),array_fill(0,count($matches[0]),'')));

        }

        return $this->_placeholder;

    }

    public function assign(string $holder='', string $value=''){

        $holder = (string) '{'.$holder.'}';

        try {
            $this->_placeholder[$holder]=(string) $value;
        } catch (Exception $e) {
            trigger_error ('Exception: '. $e->getMessage() , E_USER_ERROR );
            return false;
        }

        return true;
    }

    public function add(string $holder='', string $value=''){

        $holder = (string) '{'.$holder.'}';

        try {
            $this->_placeholder[$holder].=(string) $value;
        } catch (Exception $e) {
            trigger_error ('Exception: '. $e->getMessage() , E_USER_ERROR );
            return false;
        }

        return true;
    }

    public function display(){
        $this->_replace();
        echo $this->_output;
        flush();
    }

    protected function _replace(){
        $this->_output = '';
        foreach($this->_templates as $key=>&$content){
            $this->_output .= str_replace(array_keys($this->_placeholder),array_values($this->_placeholder),$content);
        }
    }

    public function  __destruct(){
        ob_end_flush ();
    }
}